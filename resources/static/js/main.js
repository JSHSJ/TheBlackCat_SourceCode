/**
 * Created by Josh on 11.12.2016.
 */

String.prototype.hashCode = function() {
    var hash = 0, i, chr, len;
    if (this.length === 0) return hash;
    for (i = 0, len = this.length; i < len; i++) {
        chr   = this.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
}

var solveRiddle = function () {
    var url = window.location.href;
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $.ajaxSetup({
        headers:
            {'X-CSRF-TOKEN' : token, 'X-CSRF-HEADER' : header}
    });
    $.ajax({
        type: "POST",
        url: url
    });
}

var validateKey = function(x,y){
    return x==code[y]};
