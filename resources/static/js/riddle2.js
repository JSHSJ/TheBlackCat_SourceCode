/**
 * Created by Josh on 22.02.2017.
 */
var username = document.getElementById("hiddenUserName").value;

if(username=="anonymousUser") {
    username = "User";
}
var bot = new RiveScript();
bot.loadFile([
    "/rivescript/begin.rive",
    "/rivescript/star.rive",
    "/rivescript/riddle2.rive"
],loading_done, loading_error);

function loading_done(batch_num) {
    console.log(batch_num + " has finished loading");
    createAndAppendTextNode("Hi there, " + username);
    bot.setUservar(username,"name", username);
    bot.sortReplies();
}

function loading_error (error) {
    console.log("Error when loading files: " + error);
}
var outputField = document.getElementById("riddle2-output");
var inputField = document.getElementById("riddle2-input");
var submit = document.getElementById("submit-button");

submit.onclick = function() {
    var inputText = inputField.value;
    createAndAppendTextNode(inputText, true);
    evaluateAnswer(inputText);
    inputField.value = "";
};

var evaluateAnswer = function(text) {
    var reply = bot.reply(username, text);
    createAndAppendTextNode(reply, false);
}

var createAndAppendTextNode = function(text, user) {
    var p = document.createElement("div");
    //var textNode = document.createTextNode(text);
    //p.appendChild(textNode);
    $(p).html(text);
    if (user) {
        p.style.color="#bdf400";
        p.style.align="right";
    }
    $(outputField).append(p);
}


$(document).ready(function(){$(inputField).keypress(function(e){13==e.keyCode&&$(submit).click()})});