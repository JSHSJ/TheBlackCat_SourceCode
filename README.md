This is the official source code of TheBlackCat.eu

The sites uses Spring Boot, Thymeleaf and PostgreSQL. Most configuration files are found here. I only exluded some private data for obvious reasons.

Feel free to take a look. If you have any questions, send me an email to joshstuebner[at]gmail.com or leave a comment on the blog.

Main Website: https://theblackcat.eu/
Blog: https://blog.theblackcat.eu/
