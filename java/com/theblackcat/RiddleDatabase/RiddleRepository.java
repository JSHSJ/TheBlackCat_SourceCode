package com.theblackcat.RiddleDatabase;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * JPA Access to Database
 * Can either get riddles by name or by type, returns list of matching things
 *
 * @author Josh
 * @version 1.0
 */

@Repository
public interface RiddleRepository extends CrudRepository<Riddle, Long> {
    List<Riddle> findByRiddleName(String riddleName);
    List<Riddle> findByRiddleType(String riddleType);
}
