package com.theblackcat.RiddleDatabase;

import javax.persistence.*;

/**
 * Riddle Object, one is generated for every existing riddle
 * Has SQL-Table, holds general information about riddle, not its actual contents
 *
 * @author Josh
 * @version 1.0
 */
@Entity
@Table(name = "riddles")
public class Riddle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;


    private int solutionPoints;
    private boolean reqJQueryUI;
    private String riddleName;
    private String riddleType;


    /**
     *
     * @param riddleName     name of riddle
     * @param solutionPoints amount of points solving this riddle grants
     * @param reqJQueryUI    does riddle need JQueryUI?
     * @param riddleType     type of riddle (either riddle or tutorial)
     */
    public Riddle(String riddleName, int solutionPoints, boolean reqJQueryUI, String riddleType) {
        this.solutionPoints = solutionPoints;
        this.reqJQueryUI = reqJQueryUI;
        this.riddleName = riddleName;
        this.riddleType = riddleType;

    }

    /**
     * Standard Getters and Setters
     */

    public String getRiddleName() {
        return riddleName;
    }

    public void setRiddleName(String riddleName) {
        this.riddleName = riddleName;
    }

    public int getSolutionPoints() {
        return solutionPoints;
    }

    public void setSolutionPoints(int solutionPoints) {
        this.solutionPoints = solutionPoints;
    }

    public boolean isReqJQueryUI() {
        return reqJQueryUI;
    }

    public void setReqJQueryUI(boolean reqJQueryUI) {
        this.reqJQueryUI = reqJQueryUI;
    }

    public Long getId() {

        return id;
    }

    public String getRiddleType() {
        return riddleType;
    }

    public void setRiddleType(String riddleType) {
        this.riddleType = riddleType;
    }

    public void setId(Long id) {
        this.id = id;
    }

    protected Riddle() {

    }

}
