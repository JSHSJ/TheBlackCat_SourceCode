package com.theblackcat.email;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.theblackcat.error.ControllerExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.sun.jersey.api.client.Client;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;

/**
 * Sends email with Jersey REST and Mailgun
 *
 * @author Josh
 * @version 1.0
 *  */
@Component
public class EmailSender {

    private static final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @Value("${emailSender.apiKey}")
    private String apiKey;

    @Value("${emailSender.host}")
    private String host;

    @Value("${emailSender.from}")
    private String from;


    public EmailSender() {

    }

    /**
     * The three following methods handle what type of mail is sent. They are invoked by the Controller classes,
     * add the corresponding text and settings and then call the private sendEmail helper function.
     * @see sendEmail()
     * @param to     email address to send mail to
     * @param token  verification token generated in RegistrationListener.class
     * @param name   username to address mail to
     *
     */
    public void sendVerificationMail(String to, String token, String name) {
        String url = "https://theblackcat.eu/confirmregistration?token="+token;
        String manualURL = "https://theblackcat.eu/enterregistrationtoken";
        String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/> <title>Activate your Account</title> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/></head><body style=\"margin: 0; padding: 0;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" bgcolor=\"#000\"> <tr> <td> <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;\" bgcolor=\"#ffb300\"> <tr> <td align=\"center\" bgcolor=\"#060606\" style=\"padding: 20px 0 20px 0;\"> <a href=\"https://theblackcat.eu\" target=\"_blank\"><img src=\"https://theblackcat.eu/images/logo2.png\" width=\"120\" height=\"auto\" alt=\"logo\" style=\"display: block;\"/></a> </td></tr><tr> <td bgcolor=\"#111111\" style=\"padding: 40px 30px 40px 30px;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"> <tr> <td style=\"font-size: 24px; font-weight: bold; padding: 10px 0 20px 0; color:#ffb300\"> Welcome to The Black Cat! </td></tr><tr> <td style=\"font-size: 18px; color: #bdf400\">Hi " + name + "! You're almost done with your registration. To activate your account, please click this link: </td></tr><tr> <td align=\"center\" style=\"padding: 25px 10px 25px 10px\"> <a href=\"" + url + "\" target=\"_blank\" style=\"font-size:20px;background-color: rgba(48,48,48,.8); padding: 8px; border-radius: 18px; border: 1px solid black; text-align: center; color:#ffb300; text-decoration:none\">Complete Registration</a> </td></tr><tr> <td style=\"font-size: 18px; color: #bdf400; padding: 10px 0 30px 0\"> Alternatively, you can copy the following token and paste it at <a href=\""+manualURL+"\" target=\"_blank\" style=\"text-decoration:none; color: #ffb300\"> here</a>! </td></tr><tr> <td align=\"center\" style=\"font-size: 20px; color: #ffb300;padding: 20px 10px 20px 10px; background-color: rgba(0,0,0,0.5);\">"+token+"</td></tr><tr> <td style=\"padding: 50px 0 0 0;font-size: 18px; color: #bdf400\"> I hope you're going to enjoy your stay! <br/> <br/> <span style=\"color: #ffb300\">Josh</span> </td></tr></table></td></tr><tr> <td bgcolor=\"#060606\" style=\"padding: 20px 20px 20px 20px;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"> <tr> <td align=\"center\" style=\"font-size: 14px; color: #ffb300\"> If you didn't sign up for this, please just ignore this email. You might want to find out who used your email then, though. </td></tr><tr> <td align=\"center\" style=\"font-size: 14px; color: #ffb300; padding: 10px 0 0 0\"> Copyright © 2017 The Black Cat. All rights reserved. </td></tr></table></td></tr></table> </td></tr></table></body></html>";
        String text = "Welcome to The Black Cat!\n\nHi "+name+"! You're almost done with your registration. To activate your\n account, please click this link:\n\nComplete Registration ("+url+")\n\nAlternatively, you can copy the following token and paste it\n here ("+manualURL+")!\n\n"+token+"\n\nI hope you're going to enjoy your stay!\n\nJosh\n\nIf you didn't sign up for this, please just ignore this\nemail. You might want to find out who used your email then,\nthough.\n\nCopyright © 2017 The Black Cat. All rights reserved.";
        sendEmail(to, "Verify your Registration", html, text);
    }

    public void resendVerificationMail(String to, String token, String name) {
        String url = "https://theblackcat.eu/confirmregistration?token="+token;
        String manualURL = "https://theblackcat.eu/enterregistrationtoken";
        String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/> <title>Activate your Account</title> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/></head><body style=\"margin: 0; padding: 0;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" bgcolor=\"#000\"> <tr> <td> <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;\" bgcolor=\"#ffb300\"> <tr> <td align=\"center\" bgcolor=\"#060606\" style=\"padding: 20px 0 20px 0;\"> <a href=\"https://theblackcat.eu\" target=\"_blank\"><img src=\"https://theblackcat.eu/images/logo2.png\" width=\"120\" height=\"auto\" alt=\"logo\" style=\"display: block;\"/></a> </td></tr><tr> <td bgcolor=\"#111111\" style=\"padding: 40px 30px 40px 30px;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"> <tr> <td style=\"font-size: 24px; font-weight: bold; padding: 10px 0 20px 0; color:#ffb300\"> Welcome to The Black Cat! </td></tr><tr> <td style=\"font-size: 18px; color: #bdf400\">Hi " + name + "! Here's the new verification link you requested: </td></tr><tr> <td align=\"center\" style=\"padding: 25px 10px 25px 10px\"> <a href=\"" + url + "\" target=\"_blank\" style=\"font-size:20px;background-color: rgba(48,48,48,.8); padding: 8px; border-radius: 18px; border: 1px solid black; text-align: center; color:#ffb300; text-decoration:none\">Complete Registration</a> </td></tr><tr> <td style=\"font-size: 18px; color: #bdf400; padding: 10px 0 30px 0\"> Alternatively, you can copy the following token and paste it at <a href=\""+manualURL+"\" target=\"_blank\" style=\"text-decoration:none; color: #ffb300\"> here</a>! </td></tr><tr> <td align=\"center\" style=\"font-size: 20px; color: #ffb300;padding: 20px 10px 20px 10px; background-color: rgba(0,0,0,0.5);\">"+token+"</td></tr><tr> <td style=\"padding: 50px 0 0 0;font-size: 18px; color: #bdf400\"> I hope you're going to enjoy your stay! <br/> <br/> <span style=\"color: #ffb300\">Josh</span> </td></tr></table></td></tr><tr> <td bgcolor=\"#060606\" style=\"padding: 20px 20px 20px 20px;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"> <tr> <td align=\"center\" style=\"font-size: 14px; color: #ffb300\"> If you didn't sign up for this, please just ignore this email. You might want to find out who used your email then, though. </td></tr><tr> <td align=\"center\" style=\"font-size: 14px; color: #ffb300; padding: 10px 0 0 0\"> Copyright © 2017 The Black Cat. All rights reserved. </td></tr></table></td></tr></table> </td></tr></table></body></html>";
        String text = "Welcome to The Black Cat!\n\nHi "+name+"! Here's your new verification link:\n\nComplete Registration ("+url+")\n\nAlternatively, you can copy the following token and paste it\n here ("+manualURL+")!\n\n"+token+"\n\nI hope you're going to enjoy your stay!\n\nJosh\n\nIf you didn't sign up for this, please just ignore this\nemail. You might want to find out who used your email then,\nthough.\n\nCopyright © 2017 The Black Cat. All rights reserved.";
        sendEmail(to, "Verify your Registration", html, text);
}
    public void sendPasswordRecovery(String to, String name, String token) {
        String url = "https://theblackcat.eu/resetPassword?token="+token;
        String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/> <title>Reset Password</title> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/></head><body style=\"margin: 0; padding: 0;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" bgcolor=\"#000\"> <tr> <td> <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse: collapse;\" bgcolor=\"#ffb300\"> <tr> <td align=\"center\" bgcolor=\"#060606\" style=\"padding: 20px 0 20px 0;\"> <a href=\"https://theblackcat.eu\" target=\"_blank\"><img src=\"https://theblackcat.eu/images/logo2.png\" width=\"120\" height=\"auto\" alt=\"logo\" style=\"display: block;\"/></a> </td></tr><tr> <td bgcolor=\"#111111\" style=\"padding: 40px 30px 40px 30px;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"> <tr> <td style=\"font-size: 24px; font-weight: bold; padding: 10px 0 20px 0; color:#ffb300\"> Reset your password! </td></tr><tr> <td style=\"font-size: 18px; color: #bdf400\">Hi, "+name+"! You requested a password reset! Please follow this link to change your password! </td></tr><tr> <td align=\"center\" style=\"padding: 25px 10px 25px 10px\"> <a href=\""+url+"\" target=\"_blank\" style=\"font-size:20px;background-color: rgba(48,48,48,.8); padding: 8px; border-radius: 18px; border: 1px solid black; text-align: center; color:#ffb300; text-decoration:none\">Reset password</a> </td></tr><tr> <td style=\"padding: 50px 0 0 0;font-size: 18px; color: #bdf400\"> I hope this helps! <br/> <br/> <span style=\"color: #ffb300\">Josh</span> </td></tr></table></td></tr><tr> <td bgcolor=\"#060606\" style=\"padding: 20px 20px 20px 20px;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"> <tr> <td align=\"center\" style=\"font-size: 14px; color: #ffb300\"> If you didn't request this password reset, please just ignore this email. You might want to find out who used your email then, though. </td></tr><tr> <td align=\"center\" style=\"font-size: 14px; color: #ffb300; padding: 10px 0 0 0\"> Copyright © 2017 The Black Cat. All rights reserved. </td></tr></table></td></tr></table> </td></tr></table></body></html>";
        String text = "Reset your password!\n\n Hi "+name+"! You requested a password reset! Please follow this link to change your password!:\n\nReset password ("+url+")I hope this helps!\n\nJosh\n\nIf you didn't request a password reset, please just ignore this\nemail. You might want to find out who used your email then,\nthough.\n\nCopyright © 2017 The Black Cat. All rights reserved.";
        sendEmail(to, "Reset Password", html, text);
    }

    /**
     * This helper function is called by each of the other functions in this class. It adds the constant data to a
     * Client and then returns a ClientResponse, which is a POST-Request to send the Email.
     * @param to email address to send the mail to
     * @param subject subject of the email
     * @param html HTML-Email content
     * @param text text email content
     * @return returns ClientResponse, which is a POST-Request to send the email
     */


    private ClientResponse sendEmail(String to, String subject, String html, String text) {
        Client client = Client.create();
        client.addFilter(new HTTPBasicAuthFilter("api", apiKey));
        WebResource webResource = client.resource("https://api.mailgun.net/v3/"+host+"/messages");
        MultivaluedMapImpl formData = new MultivaluedMapImpl();
        formData.add("from", "The Black Cat <"+from+">");
        formData.add("to", to);
        formData.add("subject", subject);
        formData.add("html", html);
        formData.add("text", text);
        return webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, formData);
    }


}
