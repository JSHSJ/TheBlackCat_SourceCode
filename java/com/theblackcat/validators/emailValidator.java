package com.theblackcat.validators;

import com.theblackcat.UserDatabase.User;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Email validator to check if email conforms to standards
 *
 * Checks if email is either empty or not in valid email format
 * If it conflicts with either check a corresponding error is added to the Errors class
 * which is then evaluated by the controller
 *
 * @author Josh
 * @version 1.0
 */
public class emailValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userEmail", "userEmail.empty");
        User user = (User) target;
        if(!user.getUserEmail().matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$")) {
            errors.rejectValue("userEmail", "userEmail.form");
        }

    }
}
