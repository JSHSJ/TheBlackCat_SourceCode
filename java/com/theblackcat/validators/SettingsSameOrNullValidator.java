package com.theblackcat.validators;

import com.theblackcat.UserDatabase.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.ArrayList;

/**
 * checks change settings form
 *
 * compares the changed details (email, password or message) to the existing ones or checks if they are empty
 * if they aren't empty nor match an entry is added to an arraylist, which then tells the controller, that action
 * is required
 *
 * @author Josh
 * @version 1.0
 */
public class SettingsSameOrNullValidator {
    private User ori;
    private ArrayList<String> errors;
    private BCryptPasswordEncoder encoder;

    public SettingsSameOrNullValidator(User ori) {
        this.encoder = new BCryptPasswordEncoder();
        this.ori = ori;
        this.errors = new ArrayList<>();
    }

    public ArrayList<String> validate(Object target) {
        User user = (User) target;


        if (!(user.getUserEmail().isEmpty() || user.getUserEmail().equals(ori.getUserEmail()))) {
            errors.add("email");
        }

        if (!(user.getUserPass().isEmpty() || encoder.matches(user.getUserPass(), ori.getUserPass()))) {
            errors.add("password");
        }


        if (!(user.getUserMessage().isEmpty() || user.getUserMessage().equals(ori.getUserMessage()))) {
            errors.add("message");
        }

        return this.errors;
    }
}
