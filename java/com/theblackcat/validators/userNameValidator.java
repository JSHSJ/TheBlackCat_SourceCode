package com.theblackcat.validators;

import com.theblackcat.UserDatabase.User;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * validates username
 *
 * Checks if username passes requirements of 4 to 15 characters
 * alphanumeric and _ are allowed, neither required
 * username is stored case insensitive for comparison but case sensitive to display
 *
 *
 * @author Josh
 * @version 1.0
 */
public class userNameValidator implements Validator {


    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "userName.empty");
        User user = (User) target;
        String userName = user.getUserName().toLowerCase();

        if (!userName.matches("^[a-z0-9_]{4,15}$")) {
            errors.rejectValue("userName", "userName.format");
        }

    }
}
