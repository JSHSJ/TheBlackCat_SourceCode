package com.theblackcat.validators;

import com.theblackcat.UserDatabase.User;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * validates password given by user when changing or registering
 *
 * Uses a list of the Top10000 of most common passwords to compare against
 * Also checks for empty passwords, not matching passwords and if the password passes the requirements
 * of 8 or more characters, alphanumeric and most special chars are allowed but not necessary
 *
 * @author Josh
 * @version 1.0
 */
public class passwordValidator implements Validator {

        final List<String> commonPasswords = new ArrayList<>();


    public passwordValidator() {
        Scanner reader = null;
        try {
            File file = new File("src/main/resources/static/txt/commonPasswords.txt");
            reader = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (reader.hasNext()) {
            commonPasswords.add(reader.nextLine());
        }
    }


    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userPass","userPass.empty");
        User user = (User) target;

        if (!user.getUserPass().equals(user.getUserPassConfirm())) {
            errors.rejectValue("userPassConfirm", "userPass.noMatch");
        }

        if (!user.getUserPass().matches("^[a-zA-Z0-9_!@#$%^&*-]{8,}$")) {
            errors.rejectValue("userPass", "userPass.format");
        }
        if (commonPasswords.contains(user.getUserPass())) {
            errors.rejectValue("userPass", "userPass.common");
        }
    }
}
