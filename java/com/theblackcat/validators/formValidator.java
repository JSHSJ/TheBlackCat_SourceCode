package com.theblackcat.validators;

import com.theblackcat.UserDatabase.User;
import com.theblackcat.UserDatabase.UserRepository;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Pattern;

/**
 * Validates Registration form
 *
 * Uses a set of different validators to check for validity of registration,
 * namely to validate the username, the password and the email
 *
 * @author Josh
 * @version 1.0
 */
public class formValidator implements Validator {

    private final passwordValidator passwordValidator;

    public formValidator(passwordValidator passwordValidator) {
        this.passwordValidator = passwordValidator;
    }


    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;

        ValidationUtils.invokeValidator(new userNameValidator(), user, errors);
        ValidationUtils.invokeValidator(new emailValidator(), user, errors);
        ValidationUtils.invokeValidator(this.passwordValidator, user, errors);
    }
}
