package com.theblackcat.validators;

import com.theblackcat.UserDatabase.User;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * validates user messages
 *
 * basically just checks if the user messages exceeds 200 characters, rejects it if it exceeds
 *
 * @author Josh
 * @version 1.0
 */
public class messageValidator implements Validator {


    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;
        String message = user.getUserMessage();

        if (message.length() > 200) {
            errors.rejectValue("userMessage", "userMessage.tooLong");
        }
    }
}
