package com.theblackcat.error;

import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Collection;
import java.util.Map;

/**
 * Exception that is thrown when user tries to access a page that doesn't exist
 *
 * @author Josh
 * @version 1.0
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class PageNotFoundException extends RuntimeException {

    private String path;

    public PageNotFoundException(String path) {
        super("could not find riddle " + path + "!");
        this.path = path;

    }

    public String getPath() {
        return path;
    }

}
