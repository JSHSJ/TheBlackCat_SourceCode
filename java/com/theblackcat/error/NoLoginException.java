package com.theblackcat.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception that is thrown when unauthenticated user tries to access a page that needs login
 *
 * @author Josh
 * @version 1.0
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class NoLoginException extends RuntimeException {

    private String path;

    public NoLoginException(String path) {
        super("You don't have permissions to access " + path +"!");
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}