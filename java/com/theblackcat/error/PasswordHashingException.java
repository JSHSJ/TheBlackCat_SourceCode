package com.theblackcat.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Josh
 * @version 1.0
 */
@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class PasswordHashingException extends RuntimeException{

    public PasswordHashingException(String message) {
        super(message);
    }
}
