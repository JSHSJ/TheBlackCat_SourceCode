package com.theblackcat.error;

import com.theblackcat.web.ModelManager;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Addition to ControllerExceptionHandler
 * handles all exceptions that are not explicitly defined
 *
 * @author Josh
 * @version 1.0
 */
@Controller
public class FallbackErrorController implements ErrorController {

    private static final String PATH = "/error";

    /**
     *
     * @param model
     * @param e gets exception
     * @return returns general error page with path
     */
    @RequestMapping(value = PATH)
    public String handleError(Model model, Exception e) {
        ModelManager modelManager = new ModelManager("error");
        return modelManager.processError(model, "generalError");
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
