package com.theblackcat.error;

/**
 * Exception that is thrown when user is not in database
 *
 * @author Josh
 * @version 1.0
 */

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus (HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException{

    private String userID;

    public UserNotFoundException(String userID) {
        super("could not find user " + userID + "!");
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }
}
