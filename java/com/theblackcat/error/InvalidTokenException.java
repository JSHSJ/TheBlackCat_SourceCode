package com.theblackcat.error;

import com.theblackcat.UserDatabase.RegistrationVerification.VerificationToken;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception is thrown, when the received token is invalidated in some way
 *
 * @author Josh
 * @version 1.0
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidTokenException extends RuntimeException {

    private VerificationToken token;

    public InvalidTokenException(String message, VerificationToken token) {
        super(message);
        this.token = token;
    }

    public VerificationToken getToken() {
        return this.token;
    }

}
