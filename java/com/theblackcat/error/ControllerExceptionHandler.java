package com.theblackcat.error;

import com.theblackcat.UserDatabase.RegistrationVerification.VerificationToken;
import com.theblackcat.web.ModelManager;
import org.hibernate.annotations.Target;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.FileNotFoundException;

/**
 * Exception Handler for all custom errors
 * Intercepts the configured exceptions and returns corresponding pages
 *
 * @author Josh
 * @version 1.0
 */
@ControllerAdvice
public class ControllerExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    /**
     * Exceptionhandler for UserNotFoundExceptions, when user is not found in database
     * @param exception gets custom generated Exception, which passes username to this message
     * @param model Model is gotten from request to add attributes to
     * @return returns standard ModelManager, which then throws error page with corresponding information and a 404-response
     */
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleUserNotFound(UserNotFoundException exception, Model model) {
        logger.info("UserNotFoundException: " + exception.getUserID());
        ModelManager modelManager = new ModelManager("error");
        model.addAttribute("userID", exception.getUserID());
        return modelManager.processError(model, "userNotFound");
    }

    /**
     * Exceptionhandler for PageNotFoundExceptions, when page is not mapped / does not exist
     * @param exception gets custom generated exception, which passes the path that wasn't found
     * @param model Model is gotten from request to add attributes to
     * @return returns standard ModelManager, which then throws error page with corresponding information and a 404-response
     */
    @ExceptionHandler(PageNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleUserNotFound(PageNotFoundException exception, Model model) {
        logger.info("PageNotFoundException: " + exception.getPath());
        ModelManager modelManager = new ModelManager("error");
        model.addAttribute("path", exception.getPath());
        return modelManager.processError(model, "pageNotFound");
    }

    /**
     * ExceptionHandler for NoLoginException, when user tries to access something that needs a login
     * @param exception gets custom generated exception, which passes the path that wasn't found
     * @param model Model is gotten from request to add attributes to
     * @return returns standard ModelManager, which then throws error page with corresponding information and a 401-response
     */
    @ExceptionHandler(NoLoginException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public String handleNoLogin(NoLoginException exception, Model model) {
        logger.info("NoLoginException: " + exception.getPath());
        ModelManager modelManager = new ModelManager("error");
        model.addAttribute("path", exception.getPath());
        return modelManager.processError(model, "noLogin");
    }

    @ExceptionHandler(InvalidTokenException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleInvalidToken(InvalidTokenException exception, Model model) {
        logger.info("InvalidToken: " + exception.getMessage());
        VerificationToken token = exception.getToken();
        ModelManager modelManager = new ModelManager("error");
        if (token == null) {
            model.addAttribute("errorMessage", exception.getMessage());
            return modelManager.processError(model, "badToken");
        }

        model.addAttribute("errorMessage", exception.getMessage());
        return modelManager.processError(model, "tokenExpired");
    }

}
