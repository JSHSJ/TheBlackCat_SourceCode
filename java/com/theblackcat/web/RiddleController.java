package com.theblackcat.web;

import com.theblackcat.RiddleDatabase.Riddle;
import com.theblackcat.RiddleDatabase.RiddleRepository;
import com.theblackcat.Security.CustomUserDetailService;
import com.theblackcat.UserDatabase.User;
import com.theblackcat.UserDatabase.UserRepository;
import com.theblackcat.error.PageNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.*;

/**
 * Riddle Controller Class
 * Manages riddle and tutorials mappings
 *
 * Has access to userRepository and riddleRepository
 * Maps tutorials and riddles, making them accessible to GET requests
 * Also manages solving of riddles
 *
 * @author Josh
 * @version 1.0
 */
@Controller
public class RiddleController {

    private final UserRepository userRepository;
    private final RiddleRepository riddleRepository;

    @Autowired
    private CustomUserDetailService customUserDetailService;

    @Autowired
    public RiddleController(UserRepository userRepository, RiddleRepository riddleRepository) {
        this.userRepository = userRepository;
        this.riddleRepository = riddleRepository;
    }


    /**
     * Mapping for the "start" index, showing a short description and an index for tutorials
     * If a user is logged in, it adds extra information about solved tutorials
     *
     * @param model     gets Model from request to add attributes to
     * @param principal gets Principal from request, to see if user is logged in and to manages information
     * @return          return modelManager function, which manages templates and serves website in the end
     * @see ModelManager
     */
    @GetMapping("/start")
    public String getStart(Model model, Principal principal) {
        ModelManager modelManager = new ModelManager("start");
        List<Riddle> tutorials = this.riddleRepository.findByRiddleType("Tutorial");
        model.addAttribute("tutorials", tutorials);
        model.addAttribute("riddleType", "tutorials");
        if (principal != null) {
            User user = userRepository.findByUserNameIgnoreCase(principal.getName());
            HashSet<String> solved= user.getRiddlesSolved();
            model.addAttribute("solved", solved);
        }
        return modelManager.processIndex(model);
    }

    /**
     * Mapping for the tutorials themselves, serving the tutorials on request
     * throws PageNotFountException, when riddle doesn't exist
     * @see PageNotFoundException
     *
     * @param model    gets Model from request to add attributes to
     * @param riddleId get riddleId from URI to get the right tutorial and access its information
     * @return return modelManager function, which manages templates and serves website in the end
     * @see ModelManager
     */
    @GetMapping("/start/{riddleId}")
    public String getTutorial(Model model, @PathVariable("riddleId") String riddleId) {
        if (!riddleId.matches("^[1-9]+[0-9]*$")) {
            throw new PageNotFoundException("/start/" + riddleId);
        }

        int riddleID = Integer.parseInt(riddleId);
        String riddleName = "tutorial"+riddleID;

        if(this.riddleRepository.findByRiddleName(riddleName).isEmpty()) {
            throw new PageNotFoundException("/start/" + riddleId);
        }

        Riddle riddle = this.riddleRepository.findByRiddleName(riddleName).get(0);

        model.addAttribute("reqJQueryUI", riddle.isReqJQueryUI());

        ModelManager modelManager = new ModelManager("tutorial");
        return modelManager.processRiddle(model, riddleID);
    }

    /**
     * Mapping to solve tutorials.
     * When a tutorial is solved, an automatic request is made to this mapping.
     * It checks if a user is logged in, then checks if the tutorial has been solved before and if it hasn't,
     * it adds it to the solved riddles, as well as adding points for it.
     * Throws PageNotFoundException, mainly to prevent errors
     * calls solveRiddleAndRefreshSession
     *
     * @param riddleId get riddleId from URI to get the right tutorial and access its information
     * @param principal gets Principal from request, to see if user is logged in and to manages information
     * @param request gets the request, as it is needed to refresh the user's session
     */

    @RequestMapping(value="/start/{riddleId}", method = RequestMethod.POST)
    @ResponseBody
    public void solveTutorial(@PathVariable("riddleId") String riddleId, Principal principal, HttpServletRequest request) {
        int riddleID = Integer.parseInt(riddleId);
        String riddleName = "tutorial"+riddleID;

        if(this.riddleRepository.findByRiddleName(riddleName).isEmpty()) {
            throw new PageNotFoundException("/start/" + riddleId);
        }

        Riddle riddle = this.riddleRepository.findByRiddleName(riddleName).get(0);

        if (principal != null) {
            solveRiddleAndRefreshSession(riddleName, principal.getName(), riddle.getSolutionPoints(), request);
        }
    }

    /*
    Riddle Mappings

    Very similar to start mappings, this one is for the "real" riddles
    Has same functions, only different "root" mapping
    */

    @GetMapping("/riddles")
    public String getRiddles(Model model, Principal principal) {
        ModelManager modelManager = new ModelManager("riddles");

        List<Riddle> riddles = this.riddleRepository.findByRiddleType("Riddle");
        model.addAttribute("riddleType", "riddles");
        model.addAttribute("riddles", riddles);
        if (principal != null) {
            User user = userRepository.findByUserNameIgnoreCase(principal.getName());
            HashSet<String> solved= user.getRiddlesSolved();
            model.addAttribute("solved", solved);
        }

        return modelManager.processIndex(model);
    }

    @GetMapping("/riddles/{riddleId}")
    public String getRiddle(Model model, @PathVariable("riddleId") String riddleId) {
        if (!riddleId.matches("^[1-9]+[0-9]*$")) {
            throw new PageNotFoundException("/riddles/" + riddleId);
        }

        int riddleID = Integer.parseInt(riddleId);
        String riddleName = "riddle"+riddleID;

        if(this.riddleRepository.findByRiddleName(riddleName).isEmpty()) {
            throw new PageNotFoundException("/riddle/" + riddleId);
        }

        Riddle riddle = this.riddleRepository.findByRiddleName(riddleName).get(0);


        model.addAttribute("reqJQueryUI", riddle.isReqJQueryUI());

        ModelManager modelManager = new ModelManager("riddle");
        return modelManager.processRiddle(model, riddleID);
    }

    @RequestMapping(value="/riddles/{riddleId}", method = RequestMethod.POST)
    @ResponseBody
    public void solveRiddle(@PathVariable("riddleId") String riddleId, Principal principal, HttpServletRequest request) {
        int riddleID = Integer.parseInt(riddleId);
        String riddleName = "riddle"+riddleID;

        if(this.riddleRepository.findByRiddleName(riddleName).isEmpty()) {
            throw new PageNotFoundException("/riddle/" + riddleId);
        }

        Riddle riddle = this.riddleRepository.findByRiddleName(riddleName).get(0);

        if (principal != null) {
            solveRiddleAndRefreshSession(riddleName, principal.getName(), riddle.getSolutionPoints(), request);
        }
    }

    /**
     * Helper function, which is used by riddles and tutorials.
     * It checks if the riddle has been solved. If not, it adds the riddle as solved riddle, as well as its points to
     * the currently logged in user.
     * Then the session is refreshed, to display the changes immediately.
     *
     * @param riddleName riddleName to check for in the solvedRiddle-Set
     * @param userName   userName to retrieve user
     * @param points     solution points for riddle
     * @param request    request, added by Spring, to update session
     */

    private void solveRiddleAndRefreshSession(String riddleName, String userName, int points, HttpServletRequest request) {
        User user = userRepository.findByUserNameIgnoreCase(userName);
        if (user.getRiddlesSolved() == null) {
           user.initialiseRiddlesSolved();
        }
        if (!user.getRiddlesSolved().contains(riddleName)) {
            user.solveRiddle(riddleName);
            user.setUserPoints(user.getUserPoints()+points);
            userRepository.save(user);

            UserDetails userDetails = customUserDetailService.loadUserByUsername(user.getUserName());
            Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
        }

    }
}
