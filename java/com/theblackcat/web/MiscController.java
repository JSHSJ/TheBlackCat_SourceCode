package com.theblackcat.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Misc Controller Class
 * Manages mappings for all small and miscellaneous pages
 *
 * Serves all the mappings for small pages, which are needed, but don't fit any category
 *
 * @author Josh
 * @version 1.0
 */
@Controller
public class MiscController {

    @GetMapping("/misc/{miscID}")
    public String getMiscPage(Model model, @PathVariable("miscID") String miscID) {
        ModelManager modelManager = new ModelManager("misc");
        return modelManager.processMisc(model, miscID);
    }
}