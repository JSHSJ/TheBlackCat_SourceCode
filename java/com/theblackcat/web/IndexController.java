package com.theblackcat.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Index Controller Class
 * Manages homepage, as well as about page
 *
 * Contains mapping for homepage, which is then processed by the ModelManager
 * Also contains mapping for the about page
 *
 * @author Josh
 * @version 1.0
 */
@Controller
public class IndexController {


    @GetMapping("/")
    public String getHome(Model model) {
        ModelManager modelManager = new ModelManager("home");
        return modelManager.processIndex(model);
    }


    @GetMapping("/about")
    public String getAbout(Model model) {
        ModelManager modelManager = new ModelManager("about");
        return modelManager.processIndex(model);
    }
}

