package com.theblackcat.web;

import com.theblackcat.UserDatabase.User;
import org.springframework.ui.Model;

import java.util.ArrayList;

/**
 * ModelManagerClass
 * Manages the templates and its attributes. Returns correct template, which is then resolved
 *
 * Has different methods for different templates. Adds attributes based on information given
 * Then returns a String, which is used to get the corresponding template, which is then resolved and served as a
 * web-page
 *
 * @author Josh
 * @version 1.0
 */
public class ModelManager {

    private String type;

    public ModelManager(String type) {
        this.type = type;
    }

    /*
    Processes Index-pages, which are the root directories
    Adds basic information to it, based on the ModelManager's type
     */

    public String processIndex(Model model) {

        model.addAttribute("headline", type +".headline");
        model.addAttribute("navActive" , type);
        model.addAttribute("title", type +".title");
        model.addAttribute("text", type +".text");
        String addColumns = (type.equals("home")) ?  "true" : "false";
        model.addAttribute("columns", addColumns);
        boolean reqJQuery = ((type.equals("start"))|| type.equals("riddles")) ? true : false;
        boolean levelSelection = ((type.equals("start"))|| type.equals("riddles")) ? true : false;
        model.addAttribute("reqJQuery", reqJQuery);
        model.addAttribute("levelSelection", levelSelection);
        return "indexTemplate";
    }

    /*
    Processes Riddle-pages, as well as Tutorial-pages
    Adds basic information to it, based on the riddleId, which is mainly used to retrieve text from messages.properties
     */

    public String processRiddle(Model model, int riddleId) {
        model.addAttribute("title", type + riddleId + ".title");
        String navActive = ("tutorial".equals(type)) ? "start" : "riddles";
        model.addAttribute("navActive", navActive);
        model.addAttribute("headline", type + riddleId + ".headline");
        model.addAttribute("riddleType", type);
        model.addAttribute("riddleID", riddleId);
        return "riddleTemplate";

    }

    /*
    Processes various pages, which don't need an extra method, because they don't fit any category
    Just passes on the miscID to retrieve the corresponding template fragment
     */

    public String processMisc(Model model, String miscID) {
        model.addAttribute("miscID", miscID);
        return "miscTemplate";
    }

    /*
    Processes all user-related pages, which are not the main-page
    Gets an ID to retrieve the correct page
     */

    public String processUserSubPages(Model model, String ID, String title) {
        model.addAttribute("userPart", ID);
        model.addAttribute("title", title);
        model.addAttribute("navActive", "user");
        return "userTemplate";
    }

    /*
    Processes the user page, which is a profile page for each user
    Gets a user object reference to retrieve the necessary values
     */

    public String processUser(Model model, User user) {
        model.addAttribute("title", user.getUserName());
        model.addAttribute("navActive", "user");
        model.addAttribute("username", user.getUserName());
        model.addAttribute("usermessage", user.getUserMessage());
        model.addAttribute("userpoints", ""+user.getUserPoints());
        model.addAttribute("userrank", ""+user.getUserRank());
        model.addAttribute("toString", user.toString());
        model.addAttribute("userPart", "mainPage");
        model.addAttribute("riddlesSolved", user.getRiddlesSolved());

        return "userTemplate";
    }

    public String processUserIndex(Model model) {
        model.addAttribute("userPart", "user-overview");
        model.addAttribute("title","Users");
        model.addAttribute("navActive", "user");
        model.addAttribute("reqJQuery", true);
        return "userTemplate";
    }

    /*
    Processes all error pages
     */

    public String processError(Model model, String errorID) {
        model.addAttribute("errorID", errorID);
        return "errorTemplate";
    }

}
