package com.theblackcat.UserDatabase;

import com.theblackcat.error.ControllerExceptionHandler;
import com.theblackcat.error.NoLoginException;
import com.theblackcat.error.UserNotFoundException;
import com.theblackcat.validators.SettingsSameOrNullValidator;
import com.theblackcat.validators.emailValidator;
import com.theblackcat.validators.messageValidator;
import com.theblackcat.validators.passwordValidator;
import com.theblackcat.web.ModelManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * contains mapping for most user based functions
 * does NOT manage registration
 *
 * @author Josh
 * @version 1.0
 */

@Controller
@RequestMapping("/user")
public class UserRestController {

    private final passwordValidator passwordValidator;

    private static final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);


    //Small service to perform database accesses
    @Autowired
    private UserHelperService service;

    @Autowired
    public UserRestController() {
        this.passwordValidator = new passwordValidator();
    }

    // Displays Useroverview, consisting of a user search function and a leaderboard
    //Leaderboard is updatet daily

    @GetMapping(value="/overview")
    public String getAllUsers(Model model) {
        ModelManager modelManager = new ModelManager("Users");
        model.addAttribute("topTenUser", this.service.getLeaderboard());
        return modelManager.processUserIndex(model);
    }

    // function to handle user search requests
    // Returns to overview with an error if no user is found
    // returns user page, if user is found

    @GetMapping(value="/findUser")
    public String getUser(@RequestParam("searchUser") String userName, Model model) {
        ModelManager modelManager = new ModelManager("Users");
        if (service.userNameInDatabase(userName)==false) {
            model.addAttribute("userNotFound", userName);
            model.addAttribute("topTenUser", this.service.getLeaderboard());
            return modelManager.processUserIndex(model);
        }
        return "redirect:/user/"+userName;
    }


    //Standard Get Method to display user page
    //Uses username in lower cases for URI

    @RequestMapping(value="/{userID}" ,method = RequestMethod.GET)
    public String getUser(Model model, @PathVariable String userID) {
        if(service.userNameInDatabase(userID)==false){
            throw new UserNotFoundException(userID);
        }

        ModelManager modelManager = new ModelManager("user");
        User user = service.getUser(userID);
        return modelManager.processUser(model, user);
    }


    //Get Setting Form
    //Checks if user is logged in, otherwise throws Exception

    @RequestMapping(value="/{userID}/settings", method = RequestMethod.GET)
    public String showUpdateForm(Model model, @PathVariable String userID) {
        ModelManager modelManager = new ModelManager("Settings");
        Model model1 = checkCorrectLogin(model, userID);
        return modelManager.processUserSubPages(model1, "Settings", "Settings");
    }


    // Post method to update user settings
    // Gets logged in user to compare form results to
    // then updates changed details and saves user

    @RequestMapping(value="/{userID}/settings", method = RequestMethod.POST)
    public String updateUserInfo(@ModelAttribute("user") User user,@PathVariable String userID, Model model, BindingResult result, HttpServletRequest request) {

        String userName = SecurityContextHolder.getContext().getAuthentication().getName();

        if (!userName.equalsIgnoreCase(userID)) {
            throw new NoLoginException("/user/" + userID + "/settings");
        }
        User comp = service.getUser(userName);


        SettingsSameOrNullValidator sameOrNullValidator = new SettingsSameOrNullValidator(comp);
        ArrayList<String> errors = sameOrNullValidator.validate(user);

        for (int i = 0;i < errors.size(); i++) {
            String toDo = errors.get(i);
            switch (toDo) {
                case "email": ValidationUtils.invokeValidator(new emailValidator(), user, result); break;
                case "password": ValidationUtils.invokeValidator(this.passwordValidator, user, result); break;
                case "message": ValidationUtils.invokeValidator(new messageValidator(), user, result); break;
            }
        }

        ;

        if (result.hasErrors()) {
            ModelManager modelManager = new ModelManager("Settings");
            model.addAttribute("user", user);
            return modelManager.processUserSubPages(model, "Settings", "Settings");
        }

        if (!user.getUserEmail().isEmpty()) {
            comp.setUserEmail(user.getUserEmail()); }
        if (!user.getUserPass().isEmpty()) { comp.setUserPass(user.getUserPass()); comp.setUserPassConfirm(user.getUserPass());}
        if (!user.getUserMessage().isEmpty()) { comp.setUserMessage(user.getUserMessage());}

        comp = service.setHashedPassword(comp);
        service.saveUser(comp);
        service.authenticateUserAndSetSession(userName, request);

        return "redirect:/user/"+userID;
    }

    // Gets Delete form
    // Checks if user is logged in, throws Exception otherwise

    @RequestMapping(value="/{userID}/delete", method = RequestMethod.GET)
    public String showDeleteForm(Model model, @PathVariable String userID) {
        ModelManager modelManager = new ModelManager("Delete");
        Model model1 = checkCorrectLogin(model, userID);

        return modelManager.processUserSubPages(model1, "Delete", "Delete User");
    }

    //Post method to delete user
    //Compares form results to logged in user to check if password and email match
    //Then deletes user and performs logout

    @RequestMapping(value="/{userID}/delete", method = RequestMethod.POST)
    public String deleteUser(@ModelAttribute("user") User user, @PathVariable String userID, Model model, BindingResult result, HttpServletRequest request) {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();

        if (!userName.equalsIgnoreCase(userID)) {
            throw new NoLoginException("/user/" + userID + "/settings");
        }
        User comp = service.getUser(userName);

        if (!user.getUserEmail().equals(comp.getUserEmail())) {
            result.rejectValue("userEmail", "userEmail.wrongEmail");
        }

        if (service.matchingPasswords(user, comp)==false) {
            result.rejectValue("userPass", "userPass.wrongPassword");
        }

        if (result.hasErrors()) {
            ModelManager modelManager = new ModelManager("Delete");
            Model model1 = checkCorrectLogin(model, userID);
            return modelManager.processUserSubPages(model1, "Delete", "Delete Failure");
        }

        service.deleteUser(comp);
        try {
            request.logout();
        } catch (Exception e) {
            logger.info("Trying to logout: " + e.getMessage());
        }

        return "redirect:/";
    }

    private Model checkCorrectLogin (Model model, String userID) {
        try {
            String userName = SecurityContextHolder.getContext().getAuthentication().getName();

            if (!userName.equalsIgnoreCase(userID)) {
                throw new NoLoginException("user/" + userID + "/settings");
            }
            User toEdit = service.getUser(userName);
            model.addAttribute("user", toEdit);
        } catch (Exception e) {
            logger.info("NoLoginException: " + e.getMessage());
            throw new NoLoginException("user/" + userID + "/settings");
        }
        return model;
    }

}
