package com.theblackcat.UserDatabase;

import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Interface and repository to manage user database
 *
 * @author Josh
 * @version 1.0
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findByUserNameIgnoreCase(String userName);
    User findByUserEmail(String userEmail);
    List<User> findTop10ByOrderByUserPointsDesc();
}
