package com.theblackcat.UserDatabase;


import javax.persistence.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;


/**
 * User class to hold all information about registered user
 *
 * Contains information about username, email, password and its confirmation
 * as well as a customisable user message, points, rank, solved riddles and if user is enabled
 *
 * @author Josh
 * @version 1.0
 */

@Entity
@Table(name = "users")
public class User {

    private String userName;
    private String userEmail;
    private String userPass;
    private String userPassConfirm;

    //private String userAvatar =  "/images/avatar.jpg";
    private String userMessage;
    public int userPoints ;
    private int userRank ;

    private HashSet<String> riddlesSolved;

    private boolean enabled;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_seq_gen")
    @SequenceGenerator(name = "users_seq_gen", sequenceName = "users_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    public User(User user) {
        this.userEmail = user.userEmail.toLowerCase();
        this.userName = user.userName;
        this.userPass = user.userPass;
        this.userPassConfirm = user.userPassConfirm;
        //this.userAvatar = user.userAvatar;
        this.userMessage = user.userMessage;
        this.userPoints = user.userPoints;
        this.userRank = user.userRank;
        this.riddlesSolved = user.riddlesSolved;
        this.enabled = user.enabled;

    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    //outdated user creation for testing at the moment
    public User(String userName, String userEmail, String userPass, String userPassConfirm) {
        this.userEmail = userEmail.toLowerCase();
        this.userName = userName;
        this.userPass = userPass;
        this.userPassConfirm = userPassConfirm;
        this.enabled = false;
        this.userMessage = "Hi there";
        this.userRank = 1;
        this.userPoints = 0;
        this.riddlesSolved = new HashSet<>();

    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String email) {
        this.userEmail = email.toLowerCase();
    }

    public Long getId() {
        return id;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public String getUserPassConfirm() {
        return userPassConfirm;
    }

    public void setUserPassConfirm(String userPassConfirm) {
        this.userPassConfirm = userPassConfirm;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public int getUserPoints() {
        return userPoints;
    }

    public void setUserPoints(int userPoints) {
        this.userPoints = userPoints;
    }

    public int getUserRank() {
        return userRank;
    }

    public void setUserRank(int rank) {
        this.userRank = rank;
    }

    public HashSet<String> getRiddlesSolved() {
        return riddlesSolved;
    }

    public void initialiseRiddlesSolved() {
        this.riddlesSolved = new HashSet<>();
    }

    public void solveRiddle(String riddle) {
        this.riddlesSolved.add(riddle);
    }

    @Override
    public String toString() {
        return String.format("User "+userName + " with ID " + id + ". Email: " + userEmail);
    }

    protected User() {}
}
