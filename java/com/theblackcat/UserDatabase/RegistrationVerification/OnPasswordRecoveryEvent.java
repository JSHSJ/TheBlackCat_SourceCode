package com.theblackcat.UserDatabase.RegistrationVerification;

import com.theblackcat.UserDatabase.User;
import org.springframework.context.ApplicationEvent;

/**
 * @author Josh
 * @version 1.0
 */
public class OnPasswordRecoveryEvent extends ApplicationEvent {
    /**
     * Create a new ApplicationEvent.
     *
     * @param user the object on which the event initially occurred (never {@code null})
     */

    private final User user;

    public OnPasswordRecoveryEvent(User user) {
        super(user);
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
