package com.theblackcat.UserDatabase.RegistrationVerification;

import com.theblackcat.UserDatabase.User;
import com.theblackcat.email.EmailSender;
import com.theblackcat.error.ControllerExceptionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;

import org.springframework.stereotype.Component;



/**
 * @author Josh
 * @version 1.0
 */
@Component
public class PasswordRecoveryListener implements ApplicationListener<OnPasswordRecoveryEvent> {


    @Autowired
    private EmailSender emailSender;

    @Autowired
    private TokenHelperService helperService;

    @Autowired
    private static final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @Override
    public void onApplicationEvent(OnPasswordRecoveryEvent event) {
        this.resetPassword(event);
    }

    private void resetPassword(OnPasswordRecoveryEvent event) {
        User user = event.getUser();
        VerificationToken userToken = helperService.createToken(user);
        emailSender.sendPasswordRecovery(user.getUserEmail(), user.getUserName(), userToken.getToken());
    }
}