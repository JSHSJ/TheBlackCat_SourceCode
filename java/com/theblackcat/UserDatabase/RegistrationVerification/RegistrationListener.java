package com.theblackcat.UserDatabase.RegistrationVerification;

import com.theblackcat.UserDatabase.User;
import com.theblackcat.email.EmailSender;
import com.theblackcat.error.ControllerExceptionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;


/**
 * listens for registration events
 * generates verification token and sends activation email
 *
 * @author Josh
 * @version 1.0
 */
@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {


    @Autowired
    private EmailSender emailSender;

    @Autowired
    private TokenHelperService helperService;

    @Autowired
    private MessageSource messages;

    @Autowired
    private static final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        User user = event.getUser();
        VerificationToken userToken = helperService.createToken(user);
        emailSender.sendVerificationMail(user.getUserEmail(), userToken.getToken() , user.getUserName() );
    }
}
