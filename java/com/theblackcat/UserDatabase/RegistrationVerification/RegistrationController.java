package com.theblackcat.UserDatabase.RegistrationVerification;

import com.theblackcat.UserDatabase.User;
import com.theblackcat.UserDatabase.UserHelperService;
import com.theblackcat.email.EmailSender;
import com.theblackcat.error.ControllerExceptionHandler;
import com.theblackcat.error.InvalidTokenException;
import com.theblackcat.validators.formValidator;
import com.theblackcat.validators.passwordValidator;
import com.theblackcat.web.ModelManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;

/**
 * Controller to manage everything related to user registration
 * Holds methods to GET registration, POST registration and confirmRegistration
 * Has access to tokenRepository, where verifications are stored
 * uses UserHelperService to perform basic user tasks
 *
 * @author Josh
 * @version 1.0
 */
@Controller
public class RegistrationController {

    private final com.theblackcat.validators.passwordValidator passwordValidator;

    private static final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);


    @Autowired
    private UserHelperService userService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private TokenHelperService tokenService;

    @Autowired
    private EmailSender sender;

    @Autowired
    public RegistrationController(VerificationTokenRepository tokenRepository) {
        this.passwordValidator = new passwordValidator();
    }



    /**
     * GET request mapping for registration
     * @param model gets model from request to add new "empty" user to
     * @return standard modelManager, which then serves registration form
     */

    @RequestMapping(value="/register", method = RequestMethod.GET)
    public String showRegistrationForm(Model model) {
        ModelManager modelManager = new ModelManager("Register");
        model.addAttribute("user", userService.createUser());
        return modelManager.processUserSubPages(model, "Registration", "Registration");
    }

    //Post Method for registration
    //Checks if username or email are in use and password is acceptable
    //Then saves user, but disables login
    //Creates registration event to send activation email
    //Returns errors if anything is wrong

    /**
     *Post Method for registration
     *Checks if username or email are in use and password is acceptable
     *Then saves user, but disables login
     *Creates registration event to send activation email
     *Returns errors if anything is wrong
     * @param user    receives user with information from registration form
     * @param model   see above
     * @param result  Standard Binding Results to add errors to, if needed
     * @param request Request, used to get context path
     * @return        redirects to home on successful registration, else to registration page with error messages
     */

    @RequestMapping(value="/register", method = RequestMethod.POST)
    public String registerNewUser(@ModelAttribute("user") User user, Model model, BindingResult result, HttpServletRequest request) {

        formValidator formValidator = new formValidator(passwordValidator);
        formValidator.validate(user, result);

        if (userService.userNameInDatabase(user.getUserName())) {
            result.rejectValue("userName", "userName.inUse");
        }
        if (userService.userEmailinDatabase(user.getUserEmail())) {
            result.rejectValue("userEmail", "userEmail.inUse");
        }

        if (result.hasErrors()) {
            ModelManager modelManager = new ModelManager("Registration");
            return modelManager.processUserSubPages(model, "Registration", "Registration Failure");
        }

        userService.saveNewUser(user);

        try {
            String appURL = request.getContextPath();
            OnRegistrationCompleteEvent event = new OnRegistrationCompleteEvent(user, request.getLocale(), appURL);
            this.eventPublisher.publishEvent(event);
        } catch (Exception e) {
            ModelManager modelManager = new ModelManager("Registration");
            model.addAttribute("errorMessage", "registrationError.unexpectedProblem");
            return modelManager.processUserSubPages(model, "Registration", "Registration Failure");
        }

        ModelManager modelManager = new ModelManager("emailSent");
        model.addAttribute("userEmail", user.getUserEmail());
        return modelManager.processMisc(model, "emailSent");
    }

    @GetMapping(value = "/enterregistrationtoken")
    public String confirmRegistrationManually(Model model) {
        ModelManager modelManager = new ModelManager("enterToken");
        return  modelManager.processUserSubPages(model, "enterToken", "Confirm Registration");
    }

    /**
     * Gets registration confirmation links
     * verifies token
     * if everything is correct the user is enabled and automatically logged in
     * @param request see above
     * @param model   see above
     * @param token   gets token from link, used to activate user. Is Generated upon registration
     * @return        authenticates user and redirects his user page
     */

    @GetMapping(value = "/confirmregistration")
    public String confirmRegistration(HttpServletRequest request, Model model, @RequestParam("token") String token) {

        String tokenToGet = token;
        VerificationToken verificationToken = tokenService.getTokenByString(tokenToGet);
        if (verificationToken == null) {
            throw new InvalidTokenException("registrationError.wrongToken", verificationToken);
        }

        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();

        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            throw new InvalidTokenException("registrationError.expiredToken", verificationToken);
        }

        tokenService.deleteToken(verificationToken);
        user.setEnabled(true);
        userService.saveUser(user);
        userService.authenticateUserAndSetSession(user.getUserName(), request);
        return "redirect:/user/"+user.getUserName();
    }

    @GetMapping(value ="/newtoken")
    public String getNewTokenForm(Model model) {
        ModelManager modelManager = new ModelManager("resendConfirmation");
        model.addAttribute("user", userService.createUser());
        return modelManager.processUserSubPages(model, "resendConfirmation", "New Verification Link");
    }


    @PostMapping(value = "/resendconfirmation")
    public String sendNewVerificationMail(@ModelAttribute("user") User requestuser, BindingResult result, Model model) {

        User user = userService.getUserByEmail(requestuser.getUserEmail());
        if (user == null) {
            result.rejectValue("userEmail", "userEmail.notFound");
        }else if (user.isEnabled()) {
            result.rejectValue("userEmail", "registrationError.alreadyRegistered");
        }

        if (result.hasErrors()) {
            ModelManager modelManager = new ModelManager("resendConfirmation");
            return modelManager.processUserSubPages(model, "resendConfirmation", "Registration Failure");
        }

        VerificationToken token = tokenService.getTokenByUser(user);
        tokenService.updateExpirationTime(token);
        sender.resendVerificationMail(user.getUserEmail(),token.getToken(), user.getUserName());
        ModelManager modelManager = new ModelManager("emailSent");
        model.addAttribute("userEmail", user.getUserEmail());
        return modelManager.processMisc(model, "emailSent");
    }

    //Get Login form

    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String showLoginForm(Model model) {
        ModelManager modelManager = new ModelManager("Login");
        return modelManager.processUserSubPages(model, "Login", "Login");
    }

    @GetMapping(value = "/passwordlost")
    public String getPasswordLostForm(Model model) {
        ModelManager modelManager = new ModelManager("passwordLost");
        model.addAttribute("user", userService.createUser());
        return modelManager.processUserSubPages(model, "passwordLost", "Password Recovery");
    }

    @RequestMapping(value="/passwordlost", method = RequestMethod.POST)
    public String sendRecoveryEmail(@ModelAttribute("user") User requestUser, BindingResult result, Model model) {

        User user = userService.getUserByEmail(requestUser.getUserEmail());

        if (user == null) {
            result.rejectValue("userEmail", "userEmail.notFound");
        } else if (!user.isEnabled()) {
            result.rejectValue("userEmail", "userEmail.notRegistered");
        }


        if (result.hasErrors()) {
            ModelManager modelManager = new ModelManager("passwordLost");
            return modelManager.processUserSubPages(model, "passwordLost", "Password Recovery Failure");
        }

        try {
            OnPasswordRecoveryEvent event = new OnPasswordRecoveryEvent(user);
            this.eventPublisher.publishEvent(event);
        } catch (Exception e) {
            ModelManager modelManager = new ModelManager("passwordLost");
            model.addAttribute("errorMessage", "passwordLost.unexpectedProblem");
            return modelManager.processUserSubPages(model, "passwordLost", "Password Recovery Failure");
        }

        ModelManager modelManager = new ModelManager("emailSent");
        model.addAttribute("userEmail", user.getUserEmail());
        return modelManager.processMisc(model, "emailSent");
    }

    @GetMapping(value = "/resetPassword")
    public String resetPassword(HttpServletRequest request, Model model, @RequestParam("token") String token) {

        String tokenToGet = token;
        VerificationToken verificationToken = tokenService.getTokenByString(tokenToGet);
        if (verificationToken == null) {
            throw new InvalidTokenException("passwordLost.wrongToken", verificationToken);
        }

        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();

        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            throw new InvalidTokenException("passwordLost.expiredToken", verificationToken);
        }

        tokenService.deleteToken(verificationToken);
        userService.authenticateUserAndSetSession(user.getUserName(), request);
        return "redirect:/user/"+user.getUserName()+"/settings";
    }

}
