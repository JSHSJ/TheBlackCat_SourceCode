package com.theblackcat.UserDatabase.RegistrationVerification;

import com.theblackcat.UserDatabase.User;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;
import java.util.UUID;

/**
 * @author Josh
 * @version 1.0
 */
public class OnRegistrationCompleteEvent extends ApplicationEvent {

    private final String appURL;
    private final Locale locale;
    private final User user;

    public String getAppURL() {
        return appURL;
    }

    public Locale getLocale() {
        return locale;
    }

    public User getUser() {
        return user;
    }

    public OnRegistrationCompleteEvent(User user, Locale locale, String appURL) {
        super(user);
        this.user = user;
        this.locale = locale;
        this.appURL = appURL;

    }
}
