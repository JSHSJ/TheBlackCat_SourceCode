package com.theblackcat.UserDatabase.RegistrationVerification;

import com.theblackcat.UserDatabase.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author Josh
 * @version 1.0
 */
@Service
public class TokenHelperService {

    private static final int EXPIRATION = 60 * 24;
    private static final int THIRTYDAYSOVER = 60 * 24 * 30;


    @Autowired
    private VerificationTokenRepository tokenRepository;

    //Creates new Verification Token
    public VerificationToken createToken(User user) {
        String tokenString = UUID.randomUUID().toString();
        VerificationToken token = new VerificationToken(tokenString, user, calculateExpiryDate(EXPIRATION));
        tokenRepository.save(token);
        return token;
    }

    //Returns user based off user
    public VerificationToken getTokenByUser(User user) {
        VerificationToken token = this.tokenRepository.findByUser(user);
        return token;
    }

    //Returns token based off string
    public VerificationToken getTokenByString(String token) {
        VerificationToken toReturn = this.tokenRepository.findByToken(token);
        return toReturn;
    }

    //saves or updates token
    public void saveToken(VerificationToken token) {
        this.tokenRepository.save(token);
    }

    //deletes token
    public void deleteToken(VerificationToken token) {
        this.tokenRepository.delete(token);
    }

    public void updateExpirationTime(VerificationToken token) {
        token.setExpiryDate(calculateExpiryDate(EXPIRATION));
        this.tokenRepository.save(token);
    }

    //checks if token exists in database
    public boolean tokenInDatabase(String token) {
        if(this.tokenRepository.findByToken(token) == null) {
            return false;
        }
        return true;
    }

    public void deleteAllToken(User user) {
        List<VerificationToken> allTokenForUser = this.tokenRepository.findAllByUser(user);
        for (VerificationToken token : allTokenForUser) {
            this.deleteToken(token);
        }
    }

    /**
     * calculates expiryDate, 24 hours from registration (See above 24*60)
     * @param expiryTimeInMinutes time to expire in minutes
     * @return returns Date, at which registration activation data expires
     */
    private Date calculateExpiryDate(int expiryTimeInMinutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return new Date(cal.getTime().getTime());
    }


    @Scheduled(cron = "${cron.everyFourteenDays}")
    private void deleteExpiredTokens() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        cal.add(Calendar.DAY_OF_YEAR, -30);
        List<VerificationToken> expiredTokens = this.tokenRepository.findByExpiryDateBefore(new Date(cal.getTime().getTime()));
        for (VerificationToken token : expiredTokens) {
            this.deleteToken(token);
        }
    }


}
