package com.theblackcat.UserDatabase.RegistrationVerification;

import com.theblackcat.UserDatabase.User;
import org.springframework.data.annotation.*;

import javax.persistence.*;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 * Verification Token, used to activate user after registration
 *
 * Contains randomly generated String, link to corresponding user and expiry date, which are stored in an SQL-table
 * Upon trying to confirm registration they are received and checked
 * If they are valid, user is activated
 *
 * @author Josh
 * @version 1.0
 */

@Entity
@Table(name = "verification_Token")
public class VerificationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "verification_Token_seq_gen")
    @SequenceGenerator(name = "verification_Token_seq_gen", sequenceName = "verification_Token_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    private String token;
    private boolean verified;

    @OneToOne(targetEntity = User.class)
    @JoinColumn(nullable = false, name = "users.id")
    private User user;

    private Date expiryDate;

    public VerificationToken() {
        super();
    }

    /**
     *
     * @param token randomly generated String (UUID) to identify uniqueness of activation
     * @param user  corresponding user
     */
    public VerificationToken(String token, User user, Date expiryDate) {
        super();
        this.token = token;
        this.user = user;
        this.expiryDate = expiryDate;
        this.verified = false;
    }


    /**
     * Standard Getters and Setters
     */
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public User getUser() {
        return user;

    }
}
