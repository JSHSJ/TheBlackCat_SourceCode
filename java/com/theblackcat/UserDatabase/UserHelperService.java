package com.theblackcat.UserDatabase;

import com.theblackcat.Security.CustomUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Helper class to manage user database actions
 * uses userRepository and performs various basic tasks
 * exists, so not every controller needs access to the interfaces
 *
 * @author Josh
 * @version 1.0
 */
@Service
public class UserHelperService {

        @Autowired
        private CustomUserDetailService customUserDetailService;

        @Autowired
        private  UserRepository userRepository;

        @Autowired
        private PasswordEncoder passwordEncoder;

        private List<User> top10User;



    //Creates new User
    public User createUser() {
        return new User();
    }

    //Returns user based off username
    public User getUser(String UserID) {
        User user = this.userRepository.findByUserNameIgnoreCase(UserID);
        return  user;
    }

    public User getUserByEmail(String email) {
        User user = this.userRepository.findByUserEmail(email);
        return user;
    }
    public void saveNewUser(User user) {
        String encoded = passwordEncoder.encode(user.getUserPass());
        user.setUserPass(encoded);
        user.setUserPassConfirm(encoded);
        this.userRepository.save(user);
    }

    //saves or updates user
    public void saveUser(User user) {
        this.userRepository.save(user);
    }

    //deletes user
    public void deleteUser(User user) {
        this.userRepository.delete(user);
    }

    //compares two passwords
    public boolean matchingPasswords(User toTest, User inDB) {
        return passwordEncoder.matches(toTest.getUserPass(), inDB.getUserPass());
    }

    //checks if username exists in database
    public boolean userNameInDatabase(String userID) {
        if(this.userRepository.findByUserNameIgnoreCase(userID) == null) {
            return false;
        }
        return true;
    }

    //checks if email exists in database
    public boolean userEmailinDatabase(String email) {
        if(this.userRepository.findByUserEmail(email) == null) {
            return false;
        }
        return true;
    }

    public User setHashedPassword(User user) {
        String encoded = passwordEncoder.encode(user.getUserPass());
        user.setUserPass(encoded);
        user.setUserPassConfirm(encoded);
        return user;
    }

    //Returns leaderboard
    public List<User> getLeaderboard() {
        return this.top10User;
    }

    //authenticates user for current session
    public Authentication authenticateUserAndSetSession(String username, HttpServletRequest request){

        UserDetails userDetails = customUserDetailService.loadUserByUsername(username);
        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());

        return authentication;
    }


    // Initialise leaderboard on App Startup
    @PostConstruct
    private void setLeaderboardOnStartup() {
        this.top10User = this.userRepository.findTop10ByOrderByUserPointsDesc();
    }

    // Update leaderboard every day at Midnight
    @Scheduled(cron = "${cron.dailyUpdateAtMidnight}")
    private void dailyLeaderboardUpdate() {
        this.top10User = this.userRepository.findTop10ByOrderByUserPointsDesc();
    }
}
