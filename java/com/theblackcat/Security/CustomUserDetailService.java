package com.theblackcat.Security;

import com.theblackcat.UserDatabase.User;
import com.theblackcat.UserDatabase.UserRepository;
import com.theblackcat.error.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 * Service to get userdetails from database
 *
 *
 * @author Josh
 * @version 1.0
 * */
@Service("customUserDetailService")
public class CustomUserDetailService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public CustomUserDetailService (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     *
     * @param username username(or email) to be retrieved
     * @return         returns matching custom user details, if found
     * @throws UserNotFoundException thrown when user is not found in database
     */

    @Override
    public UserDetails loadUserByUsername(String username) throws UserNotFoundException {
        User user;
        if (userRepository.findByUserNameIgnoreCase(username)!=null) {
            user = userRepository.findByUserNameIgnoreCase(username);

        } else if (userRepository.findByUserEmail(username.toLowerCase())!=null) {
            user = userRepository.findByUserEmail(username.toLowerCase());
        } else {
            throw new UserNotFoundException(username);
        }

            return new CustomUserDetails(user);

    }
}
